#!/bin/bash

LIST=("gruvbox_hard" "palenight" "palenight_dark" "dark_one" "tokyo_night" "grey_green" "lofi" "black_white")
theme=""

select ITEM in "${LIST[@]}"
do
    theme=$ITEM
break
done

echo "Selected theme: ${theme}"

####### Xresources #######
echo "Editing Xresources..."
xline="#include \"/home/$USER/.xres/${theme}\""
sed -i "1s|.*|$xline|" ~/.Xresources
xrdb ~/.Xresources && ~/.config/polybar/launch.sh
echo "Xresources updated"

####### ROFI #######
echo "Editing rofi..."
rLine="@import \"../../.xres/rofi/${theme}.rasi\""
sed -i "s|@import.*|$rLine|" ~/.config/rofi/config.rasi
echo "Rofi updated"

####### Termite #######
echo "Editing termite..."
rOldLines=$(sed '/^#==/,/^\#==/d' ~/.config/termite/config)
rContent=$(cat ~/.xres/termite/${theme})
separator=$'\n#============================\n'
total="$rOldLines$separator$rContent$separator"
#tee "/home/angeld/.config/termite/config" <<< $total
echo "$total" > "${HOME}/.config/termite/config"
echo "Termite updated"

####### I3 borders #######
echo "Editing window borders..."
cat ~/.config/i3/config.default > ~/.config/i3/config
cat "/home/$USER/.xres/i3/$theme" >> ~/.config/i3/config
i3 reload
echo "Window borders updated"

####### VIM #######
vLine="source \$HOME/.xres/vim/${theme}.vim"
sed -i "s|source \$HOME/.xres.*|$vLine|" ~/.vimrc

####### Spotify #######
echo "Editing spotify..."
killall -9 spotify
while pgrep -u $UID -x spotify >/dev/null; do sleep 1; done
spicetify config current_theme $theme
spicetify config color_scheme $theme
spicetify apply
echo "Spotify updated"

####### Nitrogen #######
echo "Wallpaper update"
nitrogen --set-zoom-fill "${HOME}/.xres/wallpapers/${theme}.png"

####### GTK #######
echo "GTK update"
declare -A list
list[gruvbox_hard]="gruvbox-gtk"
list[palenight]="palenight"
list[palenight_dark]="palenight_dark"
list[dark_one]="dark_one"

gtk_theme="gtk-theme-name=${list[$theme]}"
sed -i "s|gtk-theme-name.*|${gtk_theme}|" ~/.config/gtk-3.0/settings.ini

####### VS Code #######
echo "Editing VS Code config..."

#codeColors=$(cat $HOME/.xres/code/${theme}.json | jq '.workbench' | jq '{"workbench.colorTheme": .colorTheme, "workbench.colorCustomizations": .colorCustomizations}')
#codeColors=$(cat $HOME/.xres/code/${theme}.json | jq '.workbench' | jq '{"colorCustomizations": .colorCustomizations}')

echo $(jq -s '.[0] + .[1]' "${HOME}/.config/Code - OSS/User/settings.json" ${HOME}/.xres/code/${theme}.json) > "${HOME}/.config/Code - OSS/User/settings.json"
echo $(jq -s '.[0] + .[1]' "${HOME}/.config/Code/User/settings.json" ${HOME}/.xres/code/${theme}.json) > "${HOME}/.config/Code/User/settings.json"

#jq -r --arg codeTheme "$codeTheme" --arg customColors $codeColors '.workbench.colorTheme = $codeTheme | .workbench.colorCustomizations = $customColors' $HOME/.config/Code/User/settings.json > $HOME/.config/Code/User/tmp.json && mv $HOME/.config/Code/User/tmp.json $HOME/.config/Code/User/settings.json

echo "VS Code updated"

####### Firefox #######
cp -f "${HOME}/.xres/firefox/homepage/${theme}.css" "${HOME}/Projects/themes/firefox/startpage/colors.css"
