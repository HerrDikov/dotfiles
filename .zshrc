# Created by newuser for 5.8
echo "ZZZZZZZZZZZZZZZZZZZ   SSSSSSSSSSSSSSS HHHHHHHHH     HHHHHHHHH
Z:::::::::::::::::Z SS:::::::::::::::SH:::::::H     H:::::::H
Z:::::::::::::::::ZS:::::SSSSSS::::::SH:::::::H     H:::::::H
Z:::ZZZZZZZZ:::::Z S:::::S     SSSSSSSHH::::::H     H::::::HH
ZZZZZ     Z:::::Z  S:::::S              H:::::H     H:::::H  
        Z:::::Z    S:::::S              H:::::H     H:::::H  
       Z:::::Z      S::::SSSS           H::::::HHHHH::::::H  
      Z:::::Z        SS::::::SSSSS      H:::::::::::::::::H  
     Z:::::Z           SSS::::::::SS    H:::::::::::::::::H  
    Z:::::Z               SSSSSS::::S   H::::::HHHHH::::::H  
   Z:::::Z                     S:::::S  H:::::H     H:::::H  
ZZZ:::::Z     ZZZZZ            S:::::S  H:::::H     H:::::H  
Z::::::ZZZZZZZZ:::ZSSSSSSS     S:::::SHH::::::H     H::::::HH
Z:::::::::::::::::ZS::::::SSSSSS:::::SH:::::::H     H:::::::H
Z:::::::::::::::::ZS:::::::::::::::SS H:::::::H     H:::::::H
ZZZZZZZZZZZZZZZZZZZ SSSSSSSSSSSSSSS   HHHHHHHHH     HHHHHHHHH

" | cowsay -n -f tux | lolcat
echo ""


set -k                     # Allow comments in shell
setopt auto_cd             # cd by just typing the directory name

#
# Prompts
#
SSH_ENV=${HOME}/.ssh/environment
function start_agent {
    echo "Initialize new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}

if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

command_not_found_handler() {
    printf 'Command not found ->\033[32;05;16m %s\033[0m \n' "$0" >&2
    return 127
}

parce_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

setopt prompt_subst
PROMPT='%F{5}%F{%(?.6.1)} %f %F{8}$(parce_git_branch) |$ %f'
export SUDO_PROMPT=$'Password for ->\033[32;05;16m %u\033[0m  '


#
#  History
#

HISTSIZE=999999
SAVEHIST=999999
HISTFILE="${ZDOTDIR:-$HOME}/zsh_history"
setopt extended_history   # Record timestamp of command in HISTFILE
setopt hist_ignore_dups   # Ignore duplicated commands history list
setopt share_history      # Save command history before exiting

#
#  Autocompletion
#

setopt NO_NOMATCH   # disable some globbing
setopt complete_in_word
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' matcher-list \
    'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
    autoload -U compinit && compinit -C



# Useful aliases
alias cl='clear'
alias ..='cd ..'
alias ls='exa --icons'
alias l='ls -l'
alias lla='ls -la'
alias lt='ls --tree'
alias la='ls -a --group-directories-first --color=auto'
alias ll='ls -l --group-directories-first --color=auto'
alias cat='bat --color always --plain'
alias xdisp='DISPLAY=:1 '
alias fetish="info='n os wm sh cpu mem kern term pkgs col n' accent=4 separator='  ' fet.sh"
alias grep='grep --color=auto'

alias aurelian='yay'
# GIT
alias gs='git status'
alias gd='git diff | bat'
alias gc='git commit'
alias ga='git add'
alias gb='git branch'
alias gpl='git pull'
alias gph='git push'

# VPN
alias connectDHL='sudo openvpn --config ~/Projects/VPN/DHL/ext_angeldik.ovpn'
alias connectDEVREALM='sudo openvpn --config ~/Projects/VPN/gw-UDP4-1194-angel.dikov-config.ovpn'
 
# DOTFILES
alias config='/usr/bin/git --git-dir=${HOME}/Projects/dotfiles/ --work-tree=${HOME}'

# NAVIGATION
alias dhl='cd ~/Projects/deVRealm/DHL'
alias dispatch='cd ~/Projects/deVRealm/DHL/Scan_DispatchScreen'
alias dispatchSettings='cd ~/Projects/deVRealm/DHL/Scan_UserSettings'
alias dispatchTile='cd ~/Projects/deVRealm/DHL/Scan_Tile'
alias dispatchTable='cd ~/Projects/deVRealm/DHL/Scan_Table'
alias dispatchDropDown='cd ~/Projects/deVRealm/DHL/Scan_DropDown'


# window titles
precmd() {
    printf '\033]0;%s\007' "$(dirs)"
}

#source ~/.zsh-plugins/fzf-tab/fzf-tab.plugin.zsh

# Solarized colors
export FZF_DEFAULT_OPTS='
--color fg:#ffffff,bg:#30333d,hl:#A3BE8C,fg+:#D8DEE9,bg+:#30333d,hl+:#A3BE8C,border:#585e74
--color pointer:#f9929b,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#9ce5c0,marker:#EBCB8B
'

FZF_TAB_COMMAND=(
    fzf
    --ansi
    --expect='$continuous_trigger' # For continuous completion
    --nth=2,3 --delimiter='\x00'  # Don't search prefix
    --layout=reverse --height="''${FZF_TMUX_HEIGHT:=50%}"
    --tiebreak=begin -m --bind=tab:down,btab:up,change:top,ctrl-space:toggle --cycle
    '--query=$query'   # $query will be expanded to query string at runtime.
    '--header-lines=$#headers' # $#headers will be expanded to lines of headers at runtime
)
zstyle ':fzf-tab:*' command $FZF_TAB_COMMAND

zstyle ':completion:complete:*:options' sort false
zstyle ':fzf-tab:complete:_zlua:*' query-string input

zstyle ':fzf-tab:complete:*:*' fzf-preview '/home/javacafe01/.bin/preview.sh $realpath'

# Set PATH so it includes user's private bin directories
export PATH="${HOME}/.bin:${HOME}/.local/bin:${HOME}/.npm-global/bin/:${HOME}/.yarn/bin:${PATH}"

#clear; pwd 

