#!/bin/bash

ssid=$(iwgetid -r)
signal=$(iwconfig wlp6s0 | grep -oP '(?<=Signal level=-)[0-9]+')
ip=$(ip -o -4 addr list wlp6s0 | grep -oP '(?<=inet )[0-9.]+')
bit_rate=$(iwconfig wlp6s0 | grep -oP '(?<=Bit Rate=)[0-9.]+')
signal_res=""

if [[ 20 -gt $signal ]]
then
    signal_res="1"
elif [[ 40 -gt $signal ]]
then
    signal_res="2"
elif [[ 60 -gt $signal ]]
then
    signal_res="3"
elif [[ 80 -gt $signal ]]
then
    signal_res="4"
else
    signal_res="5"
fi

#echo "${signal} - ${ssid} - ${ip}"
echo "${signal}dBm - ${bit_rate}Mb/s - ${ssid} - ${ip}"
