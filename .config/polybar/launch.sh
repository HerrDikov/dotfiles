#!/usr/bin/env bash

chassis_type_map=(
    ""
    "Other" 
    "Unknown" 
    "Desktop" 
    "Low Profile Desktop"
    "Pizza Box"
    "Mini Tower"
    "Tower" 
    "Portable" 
    "Laptop"    
    "Notebook" 
    "Hand Held"
    "Docking Station"
    "All in One"
    "Sub Notebook"
    "Space-Saving"
    "Lunch Box"
    "Main System Chassis"
    "Expansion Chassis"
    "SubChassis"
    "Bus Expansion Chassis"
    "Peripheral Chassis"
    "Storage Chassis"
    "Rack Mount Chassis"
    "Sealed-Case PC"
)

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch main
echo "---" | tee -a /tmp/polybar1.log

deviceId=$(</sys/class/dmi/id/chassis_type)
deviceType=${chassis_type_map[$deviceId]}

echo "$deviceType"

if [ $deviceType == 'Notebook' ]
then
  deviceType='Laptop'
fi

polybar "${deviceType,,}" >>/tmp/polybar1.log 2>&1 &

echo "Bars launched..."
