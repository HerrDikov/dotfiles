#!/bin/bash

if xinput --list-props 9 | grep "Device Enabled ([0-9]*):$(printf '\t')1"
then
    xinput set-prop 9 "Device Enabled" 0
    notify-send -u "normal" --expire-time=1000 "Camera is turned off"
else
    xinput set-prop 9 "Device Enabled" 1
    notify-send -u "normal" --expire-time=1000 "Camera is turned on"
fi
