#!/bin/bash

#get device
device=$(xinput | grep -E "TouchPad")

#cut device name up until id number
deviceId=$(cut -d'=' -f 2 <<< $device)

#get id only (alternative ${deviceId:0:2})
id=$(cut -d$'\t' -f 1 <<< $deviceId)

if xinput --list-props ${id} | grep "Device Enabled ([0-9]*):$(printf '\t')1"
then
    xinput set-prop ${id} "Device Enabled" 0
    notify-send -u "normal" --expire-time=2200 "Touchpad is off"
else
    xinput set-prop ${id} "Device Enabled" 1
    notify-send -u "normal" --expire-time=2200 "Touchpad is on"
fi
