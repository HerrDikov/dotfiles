import asyncio
from multiprocessing.connection import Client
from libqtile import hook
from libqtile.lazy import lazy

import os
import subprocess

from core import widgets
from core.screens import bar

margin = widgets.bar['margin']
total = margin if type(margin) is int else sum(margin)


@hook.subscribe.startup_once
def autostart():
  home = os.path.expanduser('~/.config/qtile/autoload.sh')
  subprocess.call([home])

@hook.subscribe.startup
def startup():
  if total == 0:
    bar.window.window.set_property(
      name = 'WM_NAME',
      value = 'QTILE_BAR',
      type = 'STRING',
      format = 8,
    )

@hook.subscribe.client_new
async def client_new(client):
  await asyncio.sleep(0.01)
  lazy.spawn('notify-send -u normal ')
  if client.name == 'Spotify':
    client.togroup('6')
  if client.name == 'Firefox Developer Edition':
    client.togroup('4')
