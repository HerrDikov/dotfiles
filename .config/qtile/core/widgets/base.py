from libqtile import widget
from extras import PowerLineDecoration, RectDecoration

# This font mustn't be modified.
icon_font = 'SauceCodePro Nerd Font'

defaults = dict(
  font = 'SauceCodePro Nerd Font Medium',
  fontsize = 11,
  padding = None,
)

def base(bg: str, fg: str) -> dict:
  return {
    'background': bg,
    'foreground': fg,
  }

def battery(index:int, format=None) -> dict:
  if format is None:
    return {
      'battery': index,
      'update_interval': 1,
      'charge_char': '',
      'discharge_char': '',
      'unknown_char': '',
      'full_char': '',
      'empty_char': '',
      'notify_below': 20,
      'low_percentage': 0.2,
    }
  else:
    return {
      'battery': index,
      'format': format,
      'update_interval': 1,
      'charge_char': '',
      'discharge_char': '',
      'unknown_char': '',
      'full_char': '',
      'empty_char': '',
      'notify_below': 20,
      'low_percentage': 0.2,
    }

def decoration(side: str = 'both') -> dict:
  radius = {'left': [6, 0, 0, 6], 'right': [0, 6, 6, 0]}
  return { 'decorations': [
    RectDecoration(
      filled = True,
      radius = radius.get(side, 6),
      use_widget_background = True,
    )
  ]}

def font(fontsize: int) -> dict:
  return {
    'font': icon_font,
    'fontsize': fontsize,
  }

def icon(bg: str, fg: str) -> dict:
  return {
    **base(bg, fg),
    **font(15),
  }

def powerline(path: str | list, size: int = 9) -> dict:
  return { 'decorations': [
    PowerLineDecoration(
      path = path,
      size = size,
    )
  ]}

def spacer(bg: str) -> object:
  return widget.Spacer(background = bg)
