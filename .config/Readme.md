# Dependancies #
___

### General ###
| package     | description        			 |   |
|-------------|:----------------------------:|--:|
| dunst       | notifications preview        |   |
| notify-send | sends system notifications   |   |
| polybar     | status bar                   |   |
| compton     | window composer              |   |
| dmenu       |                              |   |
| scrot       | Command line screenshot util |   |
| termite     |                              |   |
| ranger      | File explorer                |   |

### Vim ###
	a. Plug 'vim-airline/vim-airline'
	b. Plug 'vim-airline/vim-airline-theme'
	c. drewtempelmeyer/palenight.vim
	e. Nerd Tree

### Bash ###
	a. powerline-shell

### Termite ###
	a. termite-style
