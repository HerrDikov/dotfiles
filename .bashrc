#
# ~/.bashrc
#

echo "______           _     
| ___ \         | |    
| |_/ / __ _ ___| |__  
| ___ \/ _\` / __| '_ \ 
| |_/ / (_| \__ \ | | |
\____/ \__,_|___/_| |_|

" | cowsay -n -f tux #| lolcat
echo ""

source ~/.local/share/icons-in-terminal/icons_bash.sh

[[ $- != *i* ]] && return

SSH_ENV=$HOME/.ssh/environment
export JAVA_HOME="${HOME}/Downloads/jre1.8.0_281"
#export JAVA_HOME="/var/lib/snapd/snap/android-studio/100/android-studio/jre"
export ANDROID_SDK_ROOT="${HOME}/Android/Sdk/"
export ANDROID_HOME="${HOME}/Android/Sdk/"
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/tools:$HOME/.npm-global/bin/:$HOME/.yarn/bin:$JAVA_HOME/bin:$HOME/.npm-packages/bin/

export XENVIRONMENT="${HOME}/.Xresources"

# start the ssh-agent
function start_agent {
    echo "Initializing new SSH agent..."
    # spawn ssh-agent
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}

if [ -f "${SSH_ENV}" ]; then
     . "${SSH_ENV}" > /dev/null
     ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

command_not_found_handle() {
    bold=$(tput bold)
    normal=$(tput sgr0)
    #printf "${bold}${0} says:${normal} Command not found ->\033[32;05;16m %s\033[0m \n" "$1" >&2
    printf "Command not found ->${bold}\033[32;05;16m %s\033[0m" "$1" | cowsay -d -n 
    return 127
}

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	force_color_prompt=yes
	# export PS1="\[\e[45m\] \[\e[m\]\[\e[45m\]\u\[\e[m\]\[\e[45m\]@\[\e[m\]\[\e[45m\]\h\[\e[m\]\[\e[37;45m\] \[\e[m\]\[\e[35;42m\]\[\e[m\]\[\e[42m\] \[\e[m\]\[\e[30;42m\]\W\[\e[m\]\[\e[42m\] \[\e[m\]\[\e[32m\]\[\e[m\] "
	

BRIGHT_COLOR='\e[35m'
NORMAL_COLOR='\e[0m'

parse_git_branch() {
 git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\n\[$BRIGHT_COLOR\]\W ➤  \[$NORMAL_COLOR\]"
export CLICOLOR=1


	if [[ ${EUID} == 0 ]] ; then
        PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]$(parse_git_branch)\$\[\033[00m\] '
	else
        PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]$(parse_git_branch)\$\[\033[00m\] '
	fi

	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

unset use_color safe_term match_lhs sh

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less

xhost +local:root > /dev/null 2>&1

complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

export VISUAL=vim
export EDITOR=${VISUAL}

#exec 1> >(lolcat >&2)
#exec 1> >(nms >&2)

#function _update_ps1() {
#    PS1=$(powerline-shell $?)
#}

#if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
#    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
#fi

reload_gtk_theme() {
  theme=$(gsettings get org.gnome.desktop.interface gtk-theme)
  gsettings set org.gnome.desktop.interface gtk-theme ''
  sleep 1
  gsettings set org.gnome.desktop.interface gtk-theme $theme
}

alias mount_hdd='sudo umount /dev/sda2 && sudo mount -t ntfs /dev/sda2 /disks/HDD'

alias rn_fix='echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p'
#alias create_apk='bundletool build-apks --bundle=./app-release.aab --output=./rel.apks --mode=universal'
alias create_apk="~/Projects/Personal/scripts/create_apk.sh"
alias change_theme="~/Projects/install-scripts/theme_selector.sh"

# GIT
alias gs='git status'
alias gd='git diff'
alias gc='git commit'
alias ga='git add'
alias gb='git branch'
alias gf='git fetch'
alias gpl='git pull'
alias gph='git push'

# VPN
alias connectDHL='sudo openvpn --config ~/Projects/VPN/DHL/ext_angeldik.ovpn'
alias connectDEVREALM='sudo openvpn --config ~/Projects/VPN/gw-UDP4-1194-angel.dikov-config.ovpn'

# DOTFILES
alias config='/usr/bin/git --git-dir=${HOME}/Projects/dotfiles/ --work-tree=${HOME}'

# NAVIGATION
alias hsbc='cd ~/Projects/deVRealm/HSBC/hsbc-scr-frontend'
alias dhl='cd ~/Projects/deVRealm/DHL'
alias dispatch='cd ~/Projects/deVRealm/DHL/Scan_DispatchScreen'
alias dispatchSettings='cd ~/Projects/deVRealm/DHL/Scan_UserSettings'
alias dispatchTile='cd ~/Projects/deVRealm/DHL/Scan_Tile'
alias dispatchTable='cd ~/Projects/deVRealm/DHL/Scan_Table'
alias dispatchDropDown='cd ~/Projects/deVRealm/DHL/Scan_DropDown'
alias digiburn='cd ~/Projects/deVRealm/digiburn/digiburn-poc'
alias feia='cd ~/Projects/deVRealm/FEIA'
alias la='ls -A --group-directories-first --color=auto'
alias ls='exa --icons'
#alias ls='ls --group-directories-first --color=auto'
alias ll='ls -l --group-directories-first --color=auto'
alias cl='clear'


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
