let g:lightline = { 'colorscheme': 'palenight' }
let g:airline_theme="palenight"

colorscheme palenight

set laststatus=2
hi User1 ctermfg=none ctermbg=0
hi User2 ctermfg=1 ctermbg=none
hi User3 ctermfg=0 ctermbg=1
hi User4 ctermfg=1 ctermbg=0
hi User5 ctermfg=6 ctermbg=none
hi User6 ctermfg=0 ctermbg=6
hi User7 ctermfg=0 ctermbg=none
hi User8 ctermfg=6 ctermbg=0
hi User9 ctermfg=none ctermbg=none

set statusline=
set statusline+=%2*%3*%f%4*%2*

set statusline+=%2*\ %2*\ \ %#Title#%#DiffAdd#\%{mode()}
set statusline+=\ %m%#Title#

set statusline+=%9*%=
set statusline+=\ \ %v:%l\/%L
set statusline+=\ %8*%6*\ %Y\ %5*

