set rtp+=~/.vim/plugged/tokyo_night.vim
let g:lightline = { 'colorscheme': 'tokyo_night' }
let g:airline_theme="tokyo_night"

colorscheme tokyo_night

