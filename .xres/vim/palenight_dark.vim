set rtp+=~/.vim/plugged/palenight_dark.vim
let g:lightline = { 'colorscheme': 'palenight_dark' }
let g:airline_theme="palenight_dark"

colorscheme palenight_dark

