set rtp+=~/.vim/plugged/lofi.vim
let g:lightline = { 'colorscheme': 'lofi' }
let g:airline_theme="lofi"

colorscheme lofi

