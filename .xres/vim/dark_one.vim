set rtp+=~/.vim/plugged/dark_one.vim
let g:lightline = { 'colorscheme': 'dark_one' }
let g:airline_theme="dark_one"

colorscheme dark_one

