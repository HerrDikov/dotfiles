autocmd StdinReadPre * let s:std_in=1

syntax on

noremap <silent> <C-S-Left> :vertical resize +1<CR>
noremap <silent> <C-S-Right> :vertical resize -1<CR>
noremap <silent> <C-S-Up> :resize +1 <CR>
noremap <silent> <C-S-Down> :resize -1 <CR>

let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1

call plug#begin('~/.vim/plugged')

""" WEB DEV 
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'othree/html5.vim'
Plug 'JulesWang/css.vim'

""" LIT AUTOCOMPLETE AND MORE
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'justinmk/vim-sneak'
Plug 'asvetliakov/vim-easymotion'

""" THEMES
Plug 'drewtempelmeyer/palenight.vim'
Plug 'dracula/vim',{'as':'dracula'}
Plug'larsbs/vimterial_dark'
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'


""" PREVIEW COLORS IN CSS FILES
Plug 'ap/vim-css-color'

""" AUTO PAIR BRACKETS
Plug 'jiangmiao/auto-pairs'

""" RANGER
Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}

""" FUZY FILE SEARCH
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

""" Prettier code
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'branch': 'release/0.x'
  \ }
""" Help
Plug 'liuchengxu/vim-which-key'

""" Code folding
Plug 'tmhedberg/SimpylFold'
Plug ' Konfekt/FastFold'

call plug#end()

""" mass search
let g:ackprg = 'ag --vimgrep'

""" vim-airline
let g:airline_powerline_fonts = 1

let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1

""" Enable JS syntax
let g:jsx_ext_required = 1
let g:jsx_pragma_required = 1

""" CSS colors
let g:cssColorVimDoNotMessMyUpdatetime=1

""" Palenight
let g:palenight_terminal_italics=1
set termguicolors
set nu
set ruler
set relativenumber
set background=dark

set tabstop=2 """ temporary for HSBC
set shiftwidth=2 """ temporary for HSBC
set expandtab
set foldmethod=indent
set foldlevel=99
set incsearch
set ignorecase
nnoremap <space> za

set hlsearch

nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>

source $HOME/.xres/vim/lofi.vim
source $HOME/.config/vim/modules/rnvimr.vim
source $HOME/.config/vim/modules/fzf.vim
source $HOME/.config/vim/modules/sneak.vim
source $HOME/.config/vim/modules/coc.vim

set showmode
set showcmd
set cmdheight=1

highlight LineNr ctermfg=red
autocmd FileType scss setl iskeyword+=@-@
